---
layout: handbook-page-toc
title: Code Review Guidelines
description: "Code reviews are mandatory for every merge request, you should get familiar with and follow our Code Review Guidelines."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

Code reviews are mandatory for every merge request, you should get familiar with and follow our [Code Review Guidelines](https://docs.gitlab.com/ee/development/code_review.html).

These guidelines also describe who would need to review, approve and merge your, or a community member's, merge request. They also describe the [review response time SLO's](#review-response-slo) team members have to abide by.

## Values

Every reviewer at GitLab must strive for our [reviewer values](/handbook/engineering/workflow/reviewer-values/).

## Reviewer

All GitLab engineers can, and are encouraged to, perform a code review on merge requests of colleagues and community contributors. If you want to review merge requests, you can wait until someone assigns you one, but you are also more than welcome to browse the list of open merge requests and leave any feedback or questions you may have.

You can find someone to review your merge requests by looking on the [team page](/company/team/), or on the list of [GitLab Engineering Projects](/handbook/engineering/projects/), both of which are fed by YAML files under `data/team_members/person/*`.

You can also help community contributors get their merge requests ready, by becoming a [Merge Request Coach](/job-families/expert/merge-request-coach/).

Note that while all engineers can review all merge requests, the ability to _accept_ merge requests is restricted to maintainers.

## Maintainer

Maintainers are GitLab engineers who are experts at code review, know the GitLab product and codebase very well, and are empowered to accept merge requests in one or several [GitLab Engineering Projects](/handbook/engineering/projects/).

Every project should have at least two maintainers, but most should have more. Some projects have separate maintainers for different specialties. For example, [the main GitLab codebase](https://gitlab.com/gitlab-org/gitlab) has separate maintainers for frontend, backend, and database.

Great engineers are often also great reviewers, but code review is a skill in and of itself and not every engineer, no matter their seniority, will have had the same opportunities to hone that skill. It's also important to note that a big part of being a good maintainer comes from knowing the existing product and codebase extremely well, which lets them spot inconsistencies, edge cases, or non-obvious interactions with other features that would otherwise be missed easily.

Becoming a reviewer/maintainer means you are taking on a broader responsibility beyond your immediate group. Your available capacity should be adjusted accordingly to make room for you to work effectively. There is no strict formula for this as each project comes with a different workload, but do make sure to discuss this with your manager to avoid burnout and to ensure your manager understands how this may impact your team's capacity.

To protect and ensure the quality of the codebase and the product as a whole, people become maintainers only once they have convincingly demonstrated that their reviewing skills are at a comparable level to those of existing maintainers.

As with regular reviewers, maintainers can be found on the [team page](/company/team/), or on the list of [GitLab Engineering Projects](/handbook/engineering/projects/).

### Senior+ Maintainers

For senior+ engineers, being a maintainer is part of their job family unless they have discussed with their manager or Team Member Relations in line with our [reasonable accommodation process](/handbook/people-policies/inc-usa/#reasonable-accommodation). Effective 2022-08-01, we use the following table for the timeframe expectations of maintainership:

| Description | Timeframe |
| ----------- | --------- |
| Intermediate Engineers | Maintainership is optional |
| Existing Senior+ Engineer | Existing senior+ engineers who are not already maintainers are encouraged to complete the trainee program in support of our team's productivity and motivation. There is no expected completion timeframe of the trainee program. |
| Newly hired Senior+ | During onboarding, newly hired senior+ engineers will be asked to become trainee maintainers instead of reviewers. We expect their maintainership to be complete within 12 months of their onboarding completion.
| Promotions to Senior | For engineers moving into the Senior role, we expect that they have already become a maintainer prior to promotion. |

### Meeting the reviewer/maintainer

Communication happens easier when you are familiar with the person reviewing the code. Take opportunities (for example coffee chats) to get to know reviewers to break the ice and facilitate future communication.

### How to become a project maintainer

**This applies specifically to backend, frontend and database maintainers. Other areas (docs, etc.) may have separate processes.**

Interested reviewers should check in regularly with their manager to discuss progress towards maintainership and review any recent detailed reviews, for example during their 1-on-1s. Reviewers are encouraged to also seek out a [maintainer mentor](#reviewer-mentorship-program) for further perspective on their reviews. Reviewers are encouraged to think of their eligibility for maintainership in the terms of "I could be ready at any time to be a maintainer as long as it is justified".

After each review is complete, the reviewer should write up a justification about why they believe the merge request is ready to merge. This justification is then reviewed by the maintainer and if the maintainer agrees with the justification they should add a 👍 reaction to the comment, even if they have additional non-blocking comments. The maintainer should leave a comment highlighting any blocking concerns that were missed in the initial review.

At any time, the manager/mentor may choose to open a merge request, adding the reviewer as a maintainer. This merge request should have a justification from the manager as to why the reviewer should become a maintainer. You are also welcome to open this merge request yourself at any time.

Before opening the merge request, the reviewer should:

1. Review justifications for several of the reviewer's recent merge requests.
1. Reach out to at least two of the maintainers privately for feedback on the reviewer. The reviewer may have some suggestions on who these maintainers could be.

Before merging, the manager should:

1. Mention the maintainers from the given specialty and ask them to provide feedback to the manager directly.
1. Leave the merge request open for 1 week, to give the maintainers time to provide feedback to the manager.
1. Have at least 2 approvals from existing maintainers.

**If the manager is given feedback that indicates the reviewer is not ready to become a maintainer**: the manager should close the merge request and provide the feedback directly to the reviewer to address the gaps before the reviewer is re-submitted. The earlier the manager can solicit this feedback the better.

1. Announce this change in the applicable channels listed under [keeping yourself informed section of the engineering handbook](/handbook/engineering/#keeping-yourself-informed) and `#backend_maintainers`/`#frontend_maintainers` and `#backend`/`#frontend`.
1. Post an update in the **Engineering Week-in-Review document**. The agenda is internal only, please search in Google Drive for 'Engineering Week-in-Review'.

#### After becoming a maintainer

If you've become a new maintainer, follow these instructions to request relevant permissions that will allow you to fulfill your role:

- Join the maintainer's group channel on Slack: `#frontend_maintainers`, `#backend_maintainers`, `#database_maintainers` etc.
- Ask the maintainers in your group to invite you to any maintainer-specific meeting if one exists.
- Request access to the GitLab maintainer group you belong: [frontend](https://gitlab.com/gitlab-org/maintainers/frontend), [backend](https://gitlab.com/gitlab-org/maintainers/rails-backend), or [database](https://gitlab.com/gitlab-org/maintainers/database).
- Request maintainer permissions on the projects you will act as a maintainer using the [Single Person Access Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request) issue template. Once you've created the issue, request another maintainer to grant you those permissions.

#### Learning to be a maintainer

While any reviewer may be recommended by their manager to become a maintainer at any time, reviewers who wish to become maintainers should follow a few basic steps on each review in order to get into a maintainer mindset, and learn from feedback from maintainers.

Create a merge request and indicate your role as a `project-name: reviewer` or `project-name: trainee_maintainer` in your [team member entry](https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/data/team_members/person). Assign MR to your manager for merge.

After each review, reviewers should summarize why they believe a merge request is ready to be merged:

For example:
> Looks good!  I believe this MR resolves the issue and it looks safe because the code change is relatively isolated.

> LGTM! I feel this MR is a good iteration. And it has low risk because it is behind a feature flag.

Maintainers should respond to the comment from the reviewer with a 👍 if they agree, and upon merging if there were additional comments they feel should have been caught, they should ping any reviewers so they are aware of the comments.

Some reviewers find it helpful to track their progress. This is not required, but a few ways people have done this are:

- Keep an issue with all of the various reviews and feedback comments from maintainers they have received. There are some tools to help with this type of issue:
  - https://gitlab.com/nolith/review-tanuki
  - https://gitlab.com/caalberts/review-tanuki
  - https://gitlab.com/arturoherrero/trainee
- Use an emoji to mark all MRs they received feedback from maintainers on so they are easily searchable.

### Reviewer mentorship program

Training and onboarding new maintainers is an important process. As the engineering team grows and the total number of MRs rapidly expands, the number of MR reviews per maintainer quickly becomes unsustainable.

[Recent research](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8504) has shown that the trainee process for new maintainers is hindered by several key factors:

- Reviewers self-perceived readiness and confidence
- Reviewers ability to review a substantial quantity of MRs
- Reviewers ability to review a diversity of MRs covering enough breadth across the codebase

#### Structure

1. Participation is voluntary for both maintainers and reviewers.
1. Maintainers may directly mentor up to 4 reviewers at a time.
1. Mentor / reviewer assignments are coordinated within a [maintainer_mentorship.yml](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/maintainer_mentorship.yml) file.
1. Reviewers and mentors can join the `#trainee_maintainers` Slack channel to discuss the program or ask questions.
1. Every 6 weeks the maintainer will check-in with each reviewer. This could happen async or via Coffee chat.
1. The goal of the checkin is to: review MRs, answer questions, clarify any doubts, and track readiness toward graduating.
1. Mentorship is capped at 12 months by which the reviewer should be prepared to graduate.
1. At least 1 additional check-in should be scheduled once the reviewer has graduated to celebrate the achievement and answer any further questions.
1. Make sure to remove graduated reviewers from the [maintainer_mentorship.yml](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/maintainer_mentorship.yml) file to make space for new mentees.

#### Benefits

1. It develops and expands mentorship skills among maintainers
1. Gives reviewers a regular touch point for skilling-up in deficient areas
1. Creates stronger networks amongst reviewers/maintainers than currently exists
1. Mentoring directly under a maintainer should catalyze more competence and confidence in taking ownership of the GitLab codebase.

#### After becoming a maintainer

If you’ve become a new maintainer, follow these instructions to request relevant permissions that will allow you to fulfill your role:

- Join the maintainer’s group channel on Slack: `#frontend_maintainers`, `#backend_maintainers`, etc.
- Ask the maintainers in your group to invite you to any maintainer-specific meeting if one exists.
- Request access to the GitLab maintainer group you belong: [frontend](https://gitlab.com/gitlab-org/maintainers/frontend), [backend](https://gitlab.com/gitlab-org/maintainers/rails-backend), or [database](https://gitlab.com/gitlab-org/maintainers/database).
- Request maintainer permissions on the projects you will act as a maintainer using the [Single Person Access Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request) issue template. Once you’ve created the issue, request another maintainer to grant you those permissions.

##### Transitioning away from being a reviewer/maintainer

After consultation with your manager, you may wish or need to transition away
from being a reviewer/maintainer. Regardless of the circumstances, it's
perfectly OK for this to happen! Responsibilities and workloads change; projects
evolve. So it's important to ensure your time is spent on the areas that are
most important.  To make the change official and to be removed from [reviewer roulette](https://docs.gitlab.com/ee/development/code_review.html#reviewer-roulette):

1. See the [Team Member Database](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/doc/team_database.md)
   document for how to update your YAML file.
1. Create a new MR and assign the MR to your manager.

##### Returning to become a reviewer/maintainer

After a period of transitioning away from being a reviewer/maintainer, you may
wish to return to performing these duties. To make the request official, see
the section on becoming a [Trainee maintainer](#trainee-maintainer), creating
the tracking issue and Merge Request, referencing previous tracking issue(s)
and Merge Request(s) for context. The newly created Merge Request should be
assigned to your manager for immediate review as the process can usually be
fast tracked.

### Maintainership process for smaller projects

**This is a general purpose template that projects belonging to a specific group and/or projects with less than 10 internal contributors can use to define and document their maintainership process.**

Projects may adopt these guidelines for maintainership to help grow maintainers in projects where there are not enough maintainers.

- All team members should consult the [engineering development roles](https://about.gitlab.com/job-families/engineering/development/) and become a reviewer or maintainer.
- Enable Danger Review using [`simple_roulette`](https://gitlab.com/gitlab-org/ruby/gems/gitlab-dangerfiles/-/tree/master#simple_roulette) within the project to identify MR reviewers.
- Create an [issue template](https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/.gitlab/issue_templates) for maintainer trainees. A [lightweight template](#trainee-maintainer-issue-template) is provided.
- Reduce the number of Merge Request reviews required to be considered a maintainer.
- Count work on the project itself as progress for maintainership.
- Require a maintainer mentor to help speed up the process.
- Add instructions on how to become a project maintainer to the project README.
- Curate some practice MRs based on past reviews. Create copies of the MRs in a closed state and provide the links in the project maintainership process.
- Consider creating project-specific development guidelines if they don't already exist for the project.

#### Trainee maintainer issue template

Use this lightweight template as a starting point for creating your project's trainee maintainer issue template.

```markdown
## Basic Setup

Thank you for becoming a [Project] maintainer trainee!
Please work on the list below to complete your setup.

- [ ] Change issue title to include your name: `[Project] Trainee Maintainer: [Full Name]`.
- [ ] Complete project-specific language training if the programming language is not used in the main GitLab project (example: [golang training](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/.gitlab/issue_templates/golang_training.md)).
- [ ] Review general [code review guidelines](https://docs.gitlab.com/ee/development/code_review.html).
- [ ] Review project-specific release process (if one exists).
- [ ] Join the `[project or team]` Slack channel.
- [ ] Create a merge request and indicate your role as a `project-name: reviewer` or `project-name: trainee_maintainer` in your [team member entry](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/team_database.md). Assign MR to your manager for merge.
- [ ] _Optional:_ Perform simulated reviews on practice MRs if available. Add a link to the MR and your review as a discussion in this issue.
- [ ] _Optional:_ Peer review MRs. Add a link to the MR and your review as a discussion in this issue.
- [ ] _Optional:_ Consider working on issues to gain familiarity with the project's domain and guidelines.
- [ ] _Optional:_ Reach out to an existing maintainer to [help you become](https://about.gitlab.com/handbook/engineering/workflow/code-review/#trainee-maintainer-mentorship-pilot-program) a maintainer.

## Working towards becoming a maintainer

There is no checklist here, only guidelines. Remember that there is no specific timeline on this.
In the case where you feel you are lacking more "complex" reviews, consider yourself ready enough.

Your reviews should aim to cover maintainer responsibilities as well as reviewer
responsibilities. Your approval means you think it is ready to merge.

After each MR is merged or closed, add a discussion to this issue using this
template:

<details><summary>MR discussion template</summary>

  ### (Merge request title): (Merge request URL)

  During review:

  - (List anything of note, or a quick summary. "I suggested/identified/noted...")

  Post-review:

  - (List anything of note, or a quick summary. "I missed..." or "Merged as-is")

  (Maintainer who reviewed this merge request) Please add feedback, and compare
  this review to the average maintainer review.
  
</details>

**Note:** Do not include reviews of security MRs because review feedback might
reveal security issue details.

**Tip:** There are [tools](https://about.gitlab.com/handbook/tools-and-tips/#trainee-maintainer-issue-upkeep) available to assist with this task.

## When you're ready to make it official

When reviews have accumulated, and recent reviews consistently fulfill
maintainer responsibilities, any trainee can take the next step. The trainee
should also feel free to discuss their progress with their manager or any
maintainer at any time.

1. [ ] Create a merge request updating [your team member entry](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/team_database.md) proposing yourself as a maintainer and link to this training issue. Assign to your manager.
2. [ ] Get yourself added as a Maintainer to `[project]`. Reach out to any existing maintainer.
3. [ ] Keep reviewing, start merging :metal:

/label ~"trainee maintainer"
```

### Maintainer ratios

We aim to keep the engineer : maintainer ratio under 6, for both frontend and backend. We track this in the [Engineer : Maintainer Ratio dashboard][dashboard]:

<embed width="100%" height="100%" style="min-height:850px;" src="<%= signed_periscope_url(dashboard: 475647, embed: 'v2') %>">

### Maintainer load

We aim to keep the merge request per maintainer type at a reasonable level as well. We track this in the [Merge Requests: MR Count and Ratio by FE/BE/DB][ratio]:

<embed width="100%" height="100%" style="min-height:400px;" src="<%= signed_periscope_url(dashboard: 655064, embed: 'v2') %>">

## Resident Contributor

All wider community members can, and are encouraged to, contribute in [frequent and atomic iterations](/handbook/values/#iteration). We encourage and celebrate those that contribute on a regular basis. As soon as a wider community member reaches 6 MRs within the last 3 completed months, we consider that person a Resident Contributor and they are entitled to a [review response SLO](#review-response-slo) among other benefits. These statuses are updated after each completed month. Merge requests that were created when the author was a Resident Contributor will keep the SLO commitment.

> Resident Contributor = (total merged MRs over 90 days) / 3 >= 2

## Domain Experts

Our [Code Review Guidelines](https://docs.gitlab.com/ee/development/code_review.html) states that we default to assigning reviews to team members with domain expertise.

### What makes a domain expert?

We currently don't provide rigid rules for what qualifies a team member as a domain expert and instead we use a boring solution of implicit and self-identification.

Implicit:

- Team members working in a specific stage/group (e.g. Create: Source Code) are implicitly considered domain experts for that area of the app they work on.
- Team members working on a specific feature (e.g. search) are implicitly considered domain experts for that feature.

Self-identification:

- Team members can self-identify as a domain expert for a specific feature (e.g. file uploads).
- Team members can self-identify as a domain expert for a specific technology (e.g. GraphQL), product feature (e.g. file uploads) or area of the codebase (e.g. CI).

### How to self-identify as a domain expert

The only requirement to be considered a domain expert is to have substantial experience with a specific technology, product feature or area of the codebase. We leave it up to the team member to decide whether they meet this criteria.

1. Define a new, or use an existing domain expertise key, located in [`data/domain_expertise.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/domain_expertise.yml).
1. Update your entry in your own [YAML file](https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/data/team_members/person) with a new `domain_expertise` property and list all domain expertise keys.

Example:

**domain_expertise.yml**

``` yaml
webpack:
  display_name: Webpack
  link: https://webpack.js.org/
frontend_architecture:
  display_name: Frontend Architecture
  link: https://docs.gitlab.com/ee/development/fe_guide/architecture.html
```

**your_handle.yml**

``` yaml
domain_expertise:
    - webpack
    - frontend_architecture
```

When self-identifying as a domain expert, it is recommended to assign the MR to be merged by an already established Domain Expert or a corresponding Engineering Manager.

### Where can I find a list of people with domain expertise?

The expertise of a team member can be seen on the [Engineering Projects](/handbook/engineering/projects/) page.

## Review turnaround time

Because [unblocking others is always a top priority](/handbook/values/#global-optimization),
reviewers are expected to review merge requests in a timely manner,
even when this may negatively impact their other tasks and priorities.

Doing so allows everyone involved in the merge request to iterate faster as the
context is fresh in memory, and improves contributors' experience significantly.

### Review-response SLO

To ensure swift feedback to ready-to-review code, we maintain a `Review-response` Service-level Objective (SLO).
The SLO applies to GitLab team members and resident contributors, but not to other wider community contributors.

The SLO is defined as:

> Review-response SLO = (time when review is provided) - (time MR is assigned to reviewer)

The SLO value depends on the author of the merge request:

- From GitLab team members: `Review-response` SLO < 2 business days
- From [resident contributors](#resident-contributor): `Review-response` SLO < 4 business days

If you don't think you can review a merge request in the `Review-response` SLO
time frame, let the author know as soon as possible in the comments
(no later than 36 hours after first receiving the review request)
and try to help them find another reviewer or maintainer who is able to, so that they can be unblocked
and get on with their work quickly. Remove yourself as a reviewer.

If you are at capacity and are unable to accept any more reviews until
some have been completed, communicate this through your GitLab status by setting
the 🔴 `:red_circle:` emoji and mentioning that you are at capacity in the status
text. This guides contributors to pick a different reviewer, helping us to
meet the SLO.

Of course, if you are out of office and have
[communicated](https://about.gitlab.com/handbook/paid-time-off/#communicating-your-time-off)
this through your GitLab.com Status, authors are expected to realize this and
find a different reviewer themselves.

When a merge request author has been blocked for longer than
the `Review-response` SLO, they are free to remind the reviewer through Slack or add
another reviewer.

### Managing expectation

When you are assigned to review an MR and you are not able to get to it within the `Review-response` SLO, you should leave a comment on the MR informing the author of your delayed response. If possible, you should also indicate when the author can expect your feedback or help them find an alternative reviewer.

As the author of an MR you should reassign to another reviewer or maintainer if the `Review-response` SLO has not been met and you have been unable to contact the assignee.

[Trainee backend maintainer template]: https://gitlab.com/gitlab-com/www-gitlab-com/issues/new?issuable_template=trainee-backend-maintainer
[Trainee frontend maintainer template]: https://gitlab.com/gitlab-com/www-gitlab-com/issues/new?issuable_template=trainee-frontend-maintainer
[Trainee database maintainer template]: https://gitlab.com/gitlab-com/www-gitlab-com/issues/new?issuable_template=trainee-database-maintainer
[Trainee quality maintainer template]: https://gitlab.com/gitlab-com/www-gitlab-com/issues/new?issuable_template=trainee-quality-maintainer
[1:1 meetings]: /handbook/leadership/1-1/
[dashboard]: https://app.periscopedata.com/app/gitlab/475647/Engineer-:-Maintainer-Ratio
[ratio]: https://app.periscopedata.com/app/gitlab/655064/Merge-Requests:-MR-Count-and-Ratio-by-FE-BE-DB